#!/bin/bash
#
# This is a silly little script to contain all of the knowledge
# of how to install this thing onto a debian-esque host (probably,
# Raspbian).  It's...  iffy.  And, for the moment, pretty much
# completely untested.
#
# It exists mostly because I'm too lazy to learn how to build a .deb
# package for this thing...
#
# I'll test it at some point in the future.
#

# TODO:  Need to go through this with a fresh box and see which of the
#        media-related packages are actually needed.
PACKAGES=(
    ffmpeg
    apache2
    libcamera-apps-lite
    libcamera-apps
    gstreamer1.0-tools
    gstreamer1.0-plugins-good
    gstreamer1.0-plugins-bad
    gstreamer1.0-plugins-ugly
)

# Sort out our runtime location.
if [ -f display.go ] ; then
    cd install
fi

if [ ! -f ../display.go ] ; then
    echo This script should be run from within the git repo
    echo \(or an exported version\).  You should be in either
    echo the top level directory, or the install directory.
    echo
    echo That does not appear to be the case at the moment.
    echo Therefore, I\'m taking my toys and going home.
    echo
    echo Have a nice day!
    exit 1
fi

# Sort out what host we're going to install on.  No args =
# install on localhost (and you will need permissions to
# do that!).  Arg = user@host for SSH and SCP calls.
if [ $# -lt 1 ] ; then
    cmd() {
        $*
    }
    copy() {
        cp $*
    }
    echo Installing on local host.
else
    TARGETHOST=$1
    cmd() {
        ssh root@$TARGETHOST $*
    }
    copy() {
        scp $1 root@$TARGETHOST:$2
    }
    
    echo Installing on host $1.
fi

# Build the Boredom daemon if needed.
COMPILE=1
if [ -f ../boredom ] ; then
    echo -n "Previously compiled version detected.  Recompile [y/n]? "
    read ANSWER
    case $(echo $ANSWER | tr A-Z a-z) in
        y | yes)
            echo Ok.  Compiling...
            COMPILE=1
            ;;
        *)
            echo Ok.  Skipping compile.
            COMPILE=0
            ;;
    esac
fi

if [ $COMPILE != 0 ] ; then
    ( cd .. ; make )
fi

# Install packages
cmd apt-get install ${PACKAGES[@]}

# Set up the web server for video streaming
copy m3u8-expires.conf /etc/apache2/conf-available
cmd  a2enmod  expires
cmd  a2enmod  headers
cmd  a2enconf m3u8-expires
cmd  mkdir /var/www/html/boredom
copy index.html /var/www/html
copy index.html /var/www/html/boredom

# Install boredom and custom scripts
copy boredom-capture /usr/bin
cmd  chmod 755 /usr/bin/boredom-capture
copy boredom.service /usr/lib/systemd/system
copy boredom.defaults /etc/default/boredom.sample
copy boredom-video.service /usr/lib/systemd/system
cmd  systemctl daemon-reload
cmd  systemctl enable boredom boredom-video
cmd  systemctl start boredom-video
copy ../boredom /usr/bin
cmd  chmod 755 /usr/bin/boredom

# Start 'er up!
cmd  systemctl start boredom

# And that should do it.  I hope!
