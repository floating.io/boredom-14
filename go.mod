module floating.io/boredom

go 1.18

require github.com/stianeikeland/go-rpio v4.2.0+incompatible

require github.com/rabbitmq/amqp091-go v1.7.0
