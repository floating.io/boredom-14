GOARCH := arm
GOARM  := 5
export GOARCH GOARM

all:
	go build -v
