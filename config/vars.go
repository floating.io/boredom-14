package config
/*****************************************************************************
 * Configuration For 14-Segment Display GPIO Driver
 *
 *      Author:  Steve <steve@floating.io>
 *        Date:  Mar 13 2023
 *
 *     License:  Freeware.  Or something.  Public domain, in any event.
 *
 * Description:
 *
 *   This file contains various configuration snippets for setting things
 *   up.  Primarly it is responsible for two things:
 *
 *     - Containing various configuration constants
 * 
 *     - Obtaining configuration information from the command line and/or
 *       environment variables.
 *
 *   And that's pretty much it.
 *
 ******************************************************************************/
import (
	"flag"
	"fmt"
	"os"
	"strconv"
)

type FontSelection int
	
const (
	StandardFont FontSelection = iota
	UpperCaseOnlyFont
)

const (
	Version             = "0.3.0"

	EnvPort             = "FIO_BOREDOM_PORT"
	EnvRabbitUser       = "FIO_BOREDOM_RABBIT_USERNAME"
	EnvRabbitPassword   = "FIO_BOREDOM_RABBIT_PASSWORD"
	EnvRabbitServer     = "FIO_BOREDOM_RABBIT_SERVER"
	EnvRabbitVHost      = "FIO_BOREDOM_RABBIT_VHOST"
	EnvUseUpperOnlyFont = "FIO_BOREDOM_FONT_UPPERCASE_ONLY"

	// Number of seconds that each message received is guaranteed to
	// be on the display for.  I should probably make this tunable without
	// a recompile, but I'm too lazy at the moment.
	DefaultMessageDisplayDelay = 15
)

var (
	RabbitUser                 string
	RabbitPassword             string
	RabbitServer               string
	RabbitVHost                string = "/"
	BoredomExchange            string = "fio"
	BoredomQueueName           string = "fio.boredom.messages"
	BoredomDeadLetterQueueName string = "fio.boredom.messages.dead"
	BoredomRouteKey            string = "fio.boredom.messages"
	BoredomDeadLetterRouteKey  string = "fio.boredom.messages.dead"
	SelectedFont               FontSelection = StandardFont
)


func BoolVar(target *bool, envVar, little string, dfl *bool, desc string) error {
	val, found := os.LookupEnv(envVar)
	if found {
		val, err := strconv.ParseBool(val)
		if err != nil {
			return fmt.Errorf(
				"Boolean value required for environment variable %s",
				envVar,
			)
		}
		*target = val
	}

	flag.BoolVar(target, little, *dfl, desc)
	return nil
}


func StringVar(target *string, envVar, little string, dfl *string, desc string) {
	val, found := os.LookupEnv(envVar)
	if found {
		*target = val
	}

	flag.StringVar(target, little, *dfl, desc)
}
	

func Configure() error {
	useUpperOnlyFont := false

	err := BoolVar(&useUpperOnlyFont, EnvUseUpperOnlyFont, "Fu", &useUpperOnlyFont, "Use uppercase-only display font.")
	if err != nil {
		return err
	}

	StringVar(&RabbitUser    , EnvRabbitUser    , "U", &RabbitUser      , "Username for RabbitMQ Connection")
	StringVar(&RabbitPassword, EnvRabbitPassword, "P", &RabbitPassword  , "Password for RabbitMQ Connection")
	StringVar(&RabbitServer  , EnvRabbitServer  , "S", &RabbitServer    , "Hostname of RabbitMQ Server")
	StringVar(&RabbitVHost   , EnvRabbitVHost   , "V", &RabbitVHost     , "RabbitMQ VirtualHost to Select")

	flag.Parse()

	if useUpperOnlyFont {
		SelectedFont = UpperCaseOnlyFont
	} else {
		SelectedFont = StandardFont
	}
	
	return nil
}
