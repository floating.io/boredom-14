package main

//------------------------------------------------------------------------------
// Rabbit Wrangling.
//
// This just takes messages, decodes them, and dumps them into the display
// driver.  It also implements a per-message delay, which is really pretty bogus.
// I will probably replace that with something more worthy if I ever get around
// to it.
//
// The only point in the delay is to ensure that each messages stays on the
// display for at least N seconds.
//
// Same WAT license as everything else in here.
//------------------------------------------------------------------------------

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"

	"floating.io/boredom/config"
)

var (
	server  *amqp.Connection
	channel *amqp.Channel

	// Named this way because I'm silly and didn't encapsulate anything.
	// If I had made this all properly modular it would have been better.
	rdone    chan bool
	ralldone chan bool
)

func StartRabbit() {
	rdone = make(chan bool, 1)
	ralldone = make(chan bool, 1)

	go rabbitProc(ralldone, rdone)
}

func StopRabbit() {
	rdone <- true
	<-ralldone
}

func rabbitProc(ralldone, rdone chan bool) {
	var err error
	var consumer <-chan amqp.Delivery

	delayChan := make(chan bool, 1)

	defer func() {
		if channel != nil {
			channel.Close()
		}
		if server != nil {
			server.Close()
		}
	}()

	// Yeah, don't know how people normally do this sort of thing in golang,
	// but this sort of state machine makes sense to me.  Trying to avoid
	// blocking on time.Sleep(), and trying to avoid busy-wait loops.
	const (
		Close = iota
		Connect
		Consume
		Exit
		WaitDelay
		WaitForConnectRetry
	)
	state := Connect

	for {
		switch state {
		case Close:
			// We're in a Close state.  Close the connections and
			// try to reconnect.
			if channel != nil {
				channel.Close()
				channel = nil
			}
			if server != nil {
				server.Close()
				server = nil
			}
			state = WaitForConnectRetry
			go func() {
				time.Sleep(10 * time.Second)
				delayChan <- true
			}()

		case Connect:
			// We're in a Connect state.  Try to connect and set
			// everything up to we can speak Rabbitese.
			channel, err = rabbitChannel()
			if err != nil {
				log.Printf("ERROR: %s", err.Error())
				state = Close
				break
			}

			consumer, err = channel.Consume(
				config.BoredomRouteKey,
				"boredom", // Basic consumer tag
				false,     // Manual ack
				false,     // Not exclusive
				false,     // ...
				false,     // Wait!
				nil,       // No args
			)
			if err != nil {
				log.Printf("ERROR: %s", err.Error())
				state = Close
				break
			}

			state = Consume

		case Exit:
			// Exiting.  Duh.
			if channel != nil {
				channel.Close()
				channel = nil
			}
			if server != nil {
				server.Close()
				server = nil
			}
			ralldone <- true
			return

		case Consume:
			// Consumer state.  In here we actually do the real
			// work of grabbing Rabbit messages and handling them.
			// Note that we also check on the done channel here.
			// Needed since we're in an embedded select, waiting
			// on messages.
			select {
			case <-rdone:
				state = Exit
			case message, open := <-consumer:
				// Ensure that we check the *channel* to see if
				// it's closed.  This bit me when my tunnel to
				// Amazon went down.  Endless loop!
				if !open {
					state = Close
					continue
				}

				var in struct {
					Message string `json:"message"`
					Delay   int    `json:"delay"`
				}

				err := json.Unmarshal(message.Body, &in)
				if err != nil {
					log.Printf("ERROR: %s", err.Error())
					message.Nack(false, false)
					break
				}
				SetDisplay(in.Message)
				message.Ack(false)

				if in.Delay == 0 {
					in.Delay = config.DefaultMessageDisplayDelay
				}

				go func() {
					time.Sleep(time.Second * time.Duration(in.Delay))
					delayChan <- true
				}()
				state = WaitDelay
			}

		case WaitDelay:
			// WaitDelay state.  We did something with a message, and
			// are now waiting for a timer to expire before going back
			// to consume more messages.
			select {
			case <-rdone:
				state = Exit
			case <-delayChan:
				state = Consume
			}

		case WaitForConnectRetry:
			// Similar to WaitDelay, but we're delaying prior to a
			// reconnect attempt with Rabbit.
			select {
			case <-rdone:
				state = Exit
			case <-delayChan:
				state = Connect
			}
		}
	}
}

//
// This does the acutal server connection and whatnot, or just
// returns a newly allocated Channel() if the server is already
// connected.
//
func rabbitChannel() (*amqp.Channel, error) {
	// First, set up the server if we need to.  Note that there may
	// be a channel allocation in there; this is NOT the channel we
	// will hand back, hence the defer.
	if nil == server || server.IsClosed() {
		dialString := fmt.Sprintf("amqp://%s:%s@%s%s",
			config.RabbitUser,
			config.RabbitPassword,
			config.RabbitServer,
			config.RabbitVHost,
		)
		log.Printf(
			"rabbit: connecting to: %s",
			strings.Replace(dialString, config.RabbitPassword, "xxx", 1),
		)

		var err error
		server, err = amqp.Dial(dialString)
		if err != nil {
			return nil, err
		}

		// Next, the channel we use to configure things...
		ch, err := server.Channel()
		if err != nil {
			server.Close()
			return nil, err
		}
		defer ch.Close()

		// Declare the primary fio exchange
		err = ch.ExchangeDeclare(
			config.BoredomExchange,
			"direct",
			true,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			server.Close()
			return nil, err
		}
		log.Printf("rabbit: declared exchange '%s'.", config.BoredomExchange)

		// Declare the primary queue
		q, err := ch.QueueDeclare(
			config.BoredomQueueName,
			true,  // Durable Queue
			false, // No auto-delete
			false, // Not exclusive
			false, // Wait!
			amqp.Table{
				"x-queue-mode":              "lazy",
				"x-dead-letter-exchange":    config.BoredomExchange,
				"x-dead-letter-routing-key": config.BoredomDeadLetterRouteKey,
			},
		)
		if err != nil {
			server.Close()
			return nil, err
		}
		log.Printf("rabbit: Queue [%s] has %d messages with %d consumers.",
			config.BoredomQueueName, q.Messages, q.Consumers)

		// Bind the primary queue
		err = ch.QueueBind(
			config.BoredomQueueName,
			config.BoredomRouteKey,
			config.BoredomExchange,
			false, nil,
		)
		if err != nil {
			server.Close()
			return nil, err
		}

		// Set up the Dead Letter queue, just to be on the safe side.
		q, err = ch.QueueDeclare(
			config.BoredomDeadLetterQueueName,
			true,  // Durable queue
			false, // No auto-delete
			false, // Not exclusive
			false, // Wait!
			amqp.Table{
				"x-queue-mode": "lazy",
			},
		)
		if err != nil {
			server.Close()
			return nil, err
		}
		log.Printf("rabbit: Queue [%s] has %d messages with %d consumers.",
			config.BoredomDeadLetterQueueName, q.Messages, q.Consumers)

		// Bind the dead-letter queue
		err = ch.QueueBind(
			config.BoredomDeadLetterQueueName,
			config.BoredomDeadLetterRouteKey,
			config.BoredomExchange,
			false, nil,
		)
		if err != nil {
			server.Close()
			return nil, err
		}
	}

	// Return a new channel!
	return server.Channel()
}
