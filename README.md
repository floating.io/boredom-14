# Boredom in 14 Segments

This is the code that runs the display presented in [the Boredom in 14
Segments project](https://floating.io/project/boredom-14).  See that page for
details on the hardware.

# What It Does

- Accepts input from an AMQP Queue
- Pushes that input to the display buffer
- Runs a loop that translates the display buffer into displayable
  segments on a bunch of 14-segment displays.

And that's... about it.

# How It Does It

It's written in Go.  Put simply, it slurps messages from a queue, and
each time it gets one it sends it over a channel to the display
driver, which is running in its own goroutine.  The display driver
takes those messages and updates the display buffer accordingly.

Meanwhile, it is looping and updating the display.

Basically, it pulses each digit/character in the proper configuration
for a small amount of time (~1/2880 of a second as currently
configured) and moves on to the next one.  Then after it gets them
all, it returns to the beginning.  It does this in a loop, as fast as
possible.

And that's all it is.

# Display Fonts

The display font is usable, but could still use some help.  If you
have ideas, feel free to submit a PR to update it.  I will certainly
be surprised if anyone does this, given that I doubt anyone will
duplicate this project due to its complete lack of utility.

How to do that should, in theory, be fairly self-explanatory.  See
font.go, if you need a hint.


# License

Uh, wat?  MIT.  Or public domain.  Or something.

I really don't care about this code.  Take it, fold it, spin it,
multilate it.  I don't care.
