package main

/*****************************************************************************
 * Raspberry Pi 14-Segment Display GPIO Driver
 *   - Implemented in the dumbest way possible!
 *
 *      Author:  Steve <steve@floating.io>
 *        Date:  Mar 13 2023
 *
 *     License:  Freeware.  Or something.  Public domain, in any event.
 *
 * Description:
 *
 *   This is a simplistic driver for a multiplexed 14-segment
 *   12-character display connected to the GPIO pins of a Raspberry
 *   Pi.
 *
 *   Desired connection map for HDSP-A27C displays:
 *
 *           GPIO   HDSP-A27C               GPIO   HDSP-A27C
 *     A       2       12            D01     16       16
 *     B       3       10            D02     17       11
 *     C       4        9            D03     18       16
 *     D       5        7            D04     19       11
 *     E       6        1            D05     20       16
 *     F       7       18            D06     21       11
 *     G       8       15            D07     22       16
 *     H       9       14            D08     23       11
 *     J      10        6            D09     24       16
 *     K      11        5            D10     25       11
 *     L      12        4            D11     26       16
 *     M      13        2            D12     27       11
 *     N      14       13
 *     P      15       17             ^
 *     DP      1        8             |
 *                                    +---> Digit Select (Common Cathode)
 *
 * GPIO numbering based on the information in the official documentation
 * (https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#gpio-and-the-40-pin-header)
 * so D10 is on pin 21, which is pin 40 on the GPIO header.
 *
 ****************************************************************************/

import (
	"log"
	"time"

	"github.com/stianeikeland/go-rpio"

	"floating.io/boredom/config"
)

type SegmentMapEntry struct {
	Mask uint16
	GPIO rpio.Pin
}

var (
	//
	// Segment-to-GPIO Map.
	//
	// The Mask shows which bit position the segment's data is
	// expected to be in, in a given 16-bit value.  The GPIO value
	// is, of course, the GPIO corresponding to that segment's
	// anode.
	//
	// Note that the segment map's bitmask corresponds to what we
	// expect in our silly little font array.  Funny coincidence
	// that...
	//
	segment_map = []SegmentMapEntry{
		{0x0001, 12}, // A
		{0x0002, 24}, // B
		{0x0004, 26}, // C
		{0x0008, 27}, // D
		{0x0010, 9},  // E
		{0x0020, 25}, // F
		{0x0040, 14}, // G
		{0x0080, 15}, // H
		{0x0100, 17}, // J
		{0x0200, 5},  // K
		{0x0400, 4},  // L
		{0x0800, 2},  // M
		{0x1000, 18}, // N
		{0x2000, 8},  // P
		{0x4000, 22}, // DP

	}

	//
	// Digit-to-GPIO Array.
	//
	// Describes which GPIO controls the cathode of each digit.
	//
	character_map = []rpio.Pin{
		10, 23, 7, 16, 3, 11, 6, 13, 19, 21, 20, 1,
	}

	alldone chan bool   // The reply channel for the done channel
	done    chan bool   // The done channel
	update  chan string // A channel for display updates.

	// The display font to use
	selectedFont *[]uint16 = &font14
)

//
// Display a single rune at a given digit position on the display.
//
// Note that you should ensure that you turn said digit off before
// calling this again.  The Pi only has so much drive capacity, and
// two or three digits might exceed it.  This could result in damage,
// so be careful!
//
func SetCharacterAtPosition(font *[]uint16, pos int, what uint8) {
	// Get the font entry for this rune
	glyph := (*font)[what]

	// Set up the appropriate segments for this glyph
	for _, segment := range segment_map {
		if glyph & segment.Mask != 0 {
			segment.GPIO.High()
		} else {
			segment.GPIO.Low()
		}
	}

	// Finally, connect the appropriate cathode.
	character_map[pos].Low()
}

//
// Turn off the display
//
func display_down() {
	for _, character := range character_map {
		character.High()
	}
}

//
// The display thread.
//
// This is just going to repeatedly push the display buffer out to the
// display in a scanned fashion to ensure that we can always see it.
// Really, it's a total waste of CPU.
//
func display(alldone, done chan bool, update_display chan string) {
	// Get access to the Raspberry Pi GPIOs
	err := rpio.Open()
	if err != nil {
		log.Fatal("ERROR: ", err.Error())
	}
	defer rpio.Close()

	// Configure said GPIOs
	for _, segment := range segment_map {
		segment.GPIO.PullOff()
		segment.GPIO.Output()
		segment.GPIO.Low()
	}

	for _, character := range character_map {
		character.PullOff()
		character.Output()
		character.High()
	}

	// Make sure we kill the display on exit
	defer display_down()

	// Set up initial display buffer.  Must always be at least
	// 12 characters long.
	var display_buffer string = "*FLOATINGIO*"
	var display_offset int    = 0
	var display_loopid int    = 0 // Yeah, yeah, bad.  It's a hack, damnit!
	
	// This is an evil mechanism for compressing strings with solo
	// periods in them.  Should have one entry for each entry in
	// the display buffer.
	dot_buffer := []bool{
		false, false, false, false, false, false,
		false, false, true, false, false, false,
	}

	// The actual display loop
	for {
		select {
		case <-done:
			// If we get a "done" signal, then leave.  The defer
			// above will turn off the display.
			alldone <- true
			return

		case display_buffer = <-update_display:
			//
			// If we get a display update, then update the display
			// buffer.
			//
			// This means ensure that it's at least 12 digits
			// long, the "dot buffer" is initialized correctly,
			// and then we compress extraneous periods into the
			// dot buffer as appropriate.
			//
			for len(display_buffer) < 12 {
				display_buffer += " "
			}

			dot_buffer = []bool{
				false, false, false, false, false, false,
				false, false, false, false, false, false,
			}
			for len(dot_buffer) < len(display_buffer) {
				dot_buffer = append(dot_buffer, false)
			}

			// As stated above, go through and see if we can
			// remove any dots that we can compress.  Note that we
			// don't try to compress the first digit; that would
			// be silly.  We also avoid compressing if the
			// previous digit was a dot.  Allows for elipsis,
			// that.
			if len(display_buffer) > 0 {
				ndb := string([]byte{display_buffer[0]})
				for i := 1; i < len(display_buffer); i++ {
					if display_buffer[i] == '.' && display_buffer[i-1] != '.' {
						dot_buffer[i-1] = true
					} else {
						ndb += string([]byte{display_buffer[i]})
					}
				}

				// Ensure the display buffer is at least 12 characters
				// post dot-compression.
				for len(ndb) < 12 {
					ndb += " "
				}
				display_buffer = ndb
			}


			// pre-load a long message so it will scroll in after
			// a scroll out...
			if len(display_buffer) > 12 {
				display_buffer = "            " + display_buffer
				display_offset = 12
				dot_buffer = append(
					[]bool {
						false, false, false, false, false, false,
						false, false, false, false, false, false,
					},
					dot_buffer...,
				)
			} else {
				display_offset = 0
			}

			// If we have a long message, pad it with enough space
			// that it will scroll off the display.
			if (len(display_buffer) > 12) {
				display_buffer += "             "
				dot_buffer = append(
					dot_buffer,
					false, false, false, false, false, false,
					false, false, false, false, false, false,
				)
			}
				

			continue

		default:
			// Nothing pending; scan the line and then go back to the
			// main loop.  This is the part that actually makes the
			// display light up.
			for i := 0; i < len(character_map); i++ {
				// Display the character
				SetCharacterAtPosition(selectedFont, i, display_buffer[i + display_offset])

				// If we've compressed a period, light it up.
				if dot_buffer[i + display_offset] {
					rpio.Pin(22).High()
				}

				// Leave it on the display for at least 1/2880
				// second.  This could probably be better, but I've
				// not bothered with the math.  In theory, that
				// should be roughly a 240Hz display refresh, in an
				// ideal world at least.
				time.Sleep(time.Second / (12 * 240))

				// And then turn it off.
				character_map[i].High()
			}

			display_loopid++
			if display_loopid % 20 == 0 {
				display_offset++
				if len(display_buffer) <= 12 || display_offset >= len(display_buffer)-12 {
					display_offset = 0
					display_loopid = 0  // may as well avoid an overflow...
				}
			}
			
		}
	}
}

//
// Start the display thread
//
func StartDisplay() {
	alldone = make(chan bool, 1)
	done = make(chan bool, 1)
	update = make(chan string, 1)

	switch config.SelectedFont {
	case config.StandardFont:
		selectedFont = &font14
		log.Printf("Font: standard (mixed-case)")
	case config.UpperCaseOnlyFont:
		selectedFont = &font14_upper_only
		log.Printf("Font: uppercase only")
	}

	go display(alldone, done, update)
}

//
// Set a new value for the display
//
func SetDisplay(what string) {
	update <- what
}

//
// Cancel the display thread.
//
func StopDisplay() {
	done <- true
	<-alldone
}
