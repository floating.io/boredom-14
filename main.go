package main

/*****************************************************************************
 * Raspberry Pi 14-Segment Display Manager
 *   - Implemented in the dumbest way possible!
 *
 *      Author:  Steve <steve@floating.io>
 *        Date:  Mar 13 2023
 *
 *     License:  Freeware.  Or something.  Public domain, in any event.
 *
 * Description:
 *
 *   Little toy that grabs messages of of Rabbit and shows them on a hackish
 *   bespoke 12x14-segment display.  Have fun!
 *
 ******************************************************************************/
import (
	"log"
	"os"
	"os/signal"

	"floating.io/boredom/config"
)

func main() {
	err := config.Configure()
	if err != nil {
		log.Fatal("Configuration error:  ", err.Error())
	}

	log.Printf("Boredom v%s starting.", config.Version)
	
	StartRabbit()
	StartDisplay()

	// Wait 'til we see a SIGINT.  We could expand this, but I'm...
	// not caring enough right now.
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt)
	<-sigchan

	StopDisplay()
	StopRabbit()
}
